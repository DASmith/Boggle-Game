#ifndef BOGGLE_BOGGLE_H
#define BOGGLE_BOGGLE_H

#include <string>
#include "Dictionary.h"

using namespace std;

const int BOARD_SIZE = 4;

class BoardNotInitialized {};

class Boggle {
public:
    Boggle();
    //Function: Default Constructor will create an empty boggle board with the standard boggle dictionary
    //Pre:
    //Post: An empty 4x4 boggle board is created and the standard boggle dictionary is ready to be used to check for valid words

    explicit Boggle(string filename);
    //Function: Constructor with 1 argument creates an empty boggle board with a dictionary from a given file to be used for valid words
    //Pre: A file with valid characters is desired to be used as a boggle board dictionary check
    //Post: An empty 4x4 boggle board is created and a boggle dictionary from the given file to be used to check for valid words

    void SetBoard(string board[BOARD_SIZE][BOARD_SIZE]);
    //Function: Sets a boggle board to the given characters
    //Pre: A boggle board exists
    //Post: The boggle board will now contain the given characters

    void SolveBoard(bool printBoard, ostream& output);
    //Function: Solves a boggle board and displays all possible valid words to be made from the board (Wrapper function for SolveBoardHelper)
    //Pre: A non-empty boggle board exists with a dictionary of valid words loaded
    //Post: The boggle board is solved and the user is prompted to choose whether to print the board or just see the valid words found

    void SaveSolve(string filename);
    //Function: Saves all the words from the last solve to the given file
    //Pre: A boggle board exists and has been solved
    //Post: The valid words found from the boggle board are written onto the given file


    Dictionary GetDictionary();
    //Function: returns a Dictionary object
    //Pre: A dictionary exists
    //Post: The Dictionary object and its private members are returned

    Dictionary WordsFound();
    //Function: A function that can get the number of words found used for the wordsFound Dictionary object
    //Pre: wordsFound Dictionary exists
    //Post: stores/returns the number of words found in the wordsFound Dictionary

private:
    Dictionary dict;
    Dictionary wordsFound;
    string board[BOARD_SIZE][BOARD_SIZE];
    int visited[BOARD_SIZE][BOARD_SIZE];
    void PrintBoard(ostream& output);
    //Function: displays the board[4][4] values and the visited[4][4] values side by side after a solve
    //Pre: A boggle object has been initialized and solved and user enters "y" to print the board
    //Post: The boggle board is printed with the characters used in single quotes and the visited board is printed
    //with the path of the found word given in numerical order

    void SolveBoardHelper(int row, int col, int currStep, string currPrefix, bool tprint, ostream& outFile);
    //Function: Recursive function that does most of the work in solving the boggle board
    //Pre: A valid boggle board exists and the SolveBoard function has been called
    //Post: All possible words in the given boggle board are found and displayed, with the board if desired
};

#endif //BOGGLE_BOGGLE_H

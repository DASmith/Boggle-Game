
#include "Dictionary.h"
#include "Boggle.h"

Dictionary Boggle::GetDictionary() {
    return dict;
}

Dictionary Boggle::WordsFound() {
    return wordsFound;
}

Boggle::Boggle() {                                                                      //Default Constructor
    string dictName = "dictionary.txt";
    dict.LoadDictionaryFile(dictName);                                                  //load the dictionary.txt file into the dict dictionary
    for(int i=0; i<BOARD_SIZE; i++) {
        for(int j=0; j<BOARD_SIZE; j++) {                                               //go through all 16 spaces on the board
            board[i][j] = "";                                                           //set each spot on the board to the empty string
            visited[i][j] = 0;                                                          //set all visited spots to 0
        }
    }
}

Boggle::Boggle(string filename) {                                                       //Constructor with 1 argument
    dict.LoadDictionaryFile(filename);                                                  //load the desired file into the dict dictionary
    for(int i=0; i<BOARD_SIZE; i++) {
        for (int j=0; j<BOARD_SIZE; j++) {                                              //go through all 16 spaces on the board
            board[i][j] = "";                                                           //set each spot on the board to the empty string
            visited[i][j] = 0;                                                          //set all visited spots to 0
        }
    }
}

void Boggle::SetBoard(string newBoard[4][4]) {
    for(int i=0; i<BOARD_SIZE; i++) {
        for (int j=0; j<BOARD_SIZE; j++) {                                              //go through all 16 spaces on the board
            board[i][j] = newBoard[i][j];                                               //set the current board values to the given board values
        }
    }
}

void Boggle::SolveBoard(bool printBoard, ostream &output) {
    wordsFound.MakeEmpty();                                                             //first empty the contents of the wordsFound dictionary if there is any
    bool tempPrint = printBoard;                                                        //pass the printBoard value to a temp (less confusing for me this way)
    for(int i=0; i<BOARD_SIZE; i++) {
        for (int j = 0; j < BOARD_SIZE; j++) {                                          //go through all 16 spaces on the board
            visited[i][j] = 0;                                                          //reset the values of visited to 0
        }
    }
    for(int i=0; i<4; i++) {
        for(int j=0; j<4; j++) {                                                        //go through all 16 spaces on the board
            SolveBoardHelper(i,j, 0, "", tempPrint, output);      //call the SolveBoardHelper for each spot on the board with 6 arguments
        }
    }
}

void Boggle::SaveSolve(string filename) {
    wordsFound.SaveDictionaryFile(filename);                                            //Save each component of the solved board to the wordsFound dictionary
}

void Boggle::PrintBoard(ostream &output) {
    output << "Number of Words: " << wordsFound.WordCount() << endl;                    //outputs the word count in the wordsFound dictionary
    for(int i=0; i<4; i++) {
        for(int j=0; j<4; j++) {                                                        //go through all 16 spaces on the board
            if(visited[i][j] != 0) {                                                    //if this slot on board has been visited
                output  << " '" << board[i][j] << "' ";                                 //output that character in single quotes
            } else if (visited[i][j] == 0) {                                            //if this slot on board has NOT been visited
                output << "  " << board[i][j] << "  ";                                  //just output the character on that slot
            }
        }
        output << "\t" << "  " << visited[i][0] << "    " << visited[i][1] << "    " << visited [i][2] << "    " << visited[i][3] << "  ";
        output << endl;                                                                 //(above) outputs the numerical visited values next to the board values
    }
    output << endl;
}

void Boggle::SolveBoardHelper(int row, int col, int currStep, string currPrefix, bool tPrint, ostream& outFile) {
    if(row < 0 || row >= 4 || col < 0 || col >= 4) {                                    //base case 1: check if out of bounds
       return;
    }
    if(visited[row][col] != 0) {                                                        //base case 2: check if already visited
        return;
    }
    if(!dict.IsPrefix(currPrefix)) {                                                    //base case 3: check if the currPrefix is not a prefix
        return;
    }

    currPrefix = currPrefix + board[row][col];                                          //continue building the currPrefix to eventually obtain a word
    visited[row][col] = currStep+1;                                                     //update the visited value so we know not to recheck his slot on board
    currStep = currStep+1;                                                              //iterate the currStep

    if(dict.IsWord(currPrefix) && !wordsFound.IsWord(currPrefix)) {                     //if the prefix is a valid word and hasn't already been found
        wordsFound.AddWord(currPrefix);                                                 //add the word to the wordsFound dictionary so we don't double count
        if(tPrint) {                                                                    //if user wishes to the print the board
            outFile << "Word: " << currPrefix << endl;                                  //must output the words here because currPrefix will be changing throughout
            PrintBoard(outFile);                                                     //call the PrintBoard function
        } else {                                                                        //if the user does not want to print the board
            outFile << wordsFound.WordCount() << "\t" << currPrefix << endl;            //just print the word next to the # so the user knows how many were found
        }
    }

    SolveBoardHelper(row-1,col, currStep, currPrefix, tPrint, outFile);         //North
    SolveBoardHelper(row-1,col+1, currStep, currPrefix, tPrint,outFile);    //North East
    SolveBoardHelper(row,col+1, currStep, currPrefix, tPrint, outFile);          //East
    SolveBoardHelper(row+1,col+1, currStep, currPrefix, tPrint, outFile);   //South East
    SolveBoardHelper(row+1,col, currStep, currPrefix, tPrint, outFile);         //South
    SolveBoardHelper(row+1,col-1, currStep, currPrefix, tPrint, outFile);   //South West
    SolveBoardHelper(row,col-1, currStep, currPrefix, tPrint, outFile);          //West
    SolveBoardHelper(row-1,col-1, currStep, currPrefix, tPrint, outFile);   //North West

    visited[row][col] = 0;                                                   //reset the visited spot to 0 because this slot was not going to make a valid word
}





#include "Dictionary.h"
#include <fstream>

Dictionary::Dictionary(const Dictionary &otherDict) {                       //Copy Constructor
    root = new Node;
    numWords = 0;
    *this = otherDict;                                                      //use overloaded = to change dictionaries
}

Dictionary::Dictionary() {                                                  //Default Constructor
    root = new Node;
    root->isAWord = false;
    numWords = 0;
}

Dictionary::~Dictionary() {                                                 //Deconstructor - just calls destroyHelper function
    destroyHelper(root);
}

Dictionary::Dictionary(string filename) {                                   //Constructor with one argument
    root = new Node;
    root->isAWord = false;
    numWords = 0;
    LoadDictionaryFile(filename);                                           //Makes a new dictionary and loads information from a file into it
}

Dictionary &Dictionary::operator=(const Dictionary &otherDict) {            //Overload operator on = for the copy constructor (wrapper function)
    this->MakeEmpty();                                                      //First make the current dictionary empty
    numWords = otherDict.numWords;                                          //set the number of words equal to the dictionary to be copied
    copyHelper(root, otherDict.root);                                    //Call the copyHelper function
    return *this;                                                           //After everything is copied over, return the new dictionary
}

void Dictionary::LoadDictionaryFile(string filename) {
    ifstream newWords;
    string temp;                                                            //create a string data type to take in string from a file

    newWords.open(filename);                                                //open the file
    if(!newWords.is_open()) {                                               //if the file cannot be opened,
        throw DictionaryError(filename+"failed to open");              //throw an error
    }

    while(newWords >> temp) {                                               //while there are still strings in the file, read them
        AddWord(temp);                                                      //add each word to the dictionary from the file 1 by 1
    }
    newWords.close();                                                       //Close the file
}

void Dictionary::SaveDictionaryFile(string filename) {                      //Wrapper function for the SaveDictionaryHelper function
    ofstream saveFile;
    saveFile.open(filename);                                                //Open the file to be saved
    if(!saveFile.is_open()) {                                               //if the file cannot be opened,
       throw DictionaryError(filename+"failed to open");               //throw an error
    }

    SaveDictionaryHelper(root, "", saveFile);                  //Call the SaveDictionaryHelper function
    saveFile.close();                                                       //Close the file
}

void Dictionary::AddWord(string word) {
    Node* currNode = root;                                                  //set a traveling node pointer to the root
    for(int i=0; i<word.size(); i++) {                                      //go through each letter of a given string
        int letterVal = (int)word[i] - (int)'a';                            //calculate the integer value for each letter and make it range from 0-25
        if(letterVal < 0 || letterVal > 25) {                               //check for invalid characters
            throw DictionaryError("Invalid character");                //throw an error message if an invalid character was given
        }
        if(currNode->branch[letterVal] == nullptr) {                        //if we reached the end of a branch
            currNode->branch[letterVal] = new Node;                         //create a new node in this location
            currNode->branch[letterVal]->isAWord = false;                   //set the isAWord member to false because we haven't completed the word yet
        }
        currNode = currNode->branch[letterVal];                             //move on the the next character of the given word
    }
    currNode->isAWord = true;                                               //we have no more characters left in the given word, so set the isAWord member to true
    numWords++;                                                             //update the number of words in our dictionary
}

void Dictionary::MakeEmpty() {                                              //Wrapper function for destroyHelper
    destroyHelper(root);                                                    //Call the destroyHelper function which does most of the work
    root = new Node;                                                        //reset the root to a new node because we are just making the tree empty
    numWords = 0;                                                           //reset the number of words to 0
}

bool Dictionary::IsWord(string word) {
    Node* currNode = root;                                                 //set a traveling node pointer to the root
    for (int i = 0; i < word.size(); i++) {                                //go through each letter of a given word
        int letterVal = (int)word[i] - (int)'a';                           //calculate the integer value for each letter and make it range from 0-25
        if (letterVal < 0 || letterVal > 25) {                             //check for invalid characters
            throw DictionaryError("Invalid character");               //throw an error message if an invalid character was given
        }
        if(currNode->branch[letterVal] == nullptr) {                       //if we reached the end of a branch but still have more letters to go
              return false;                                                //then this must not be a word, so return false
        }
        currNode = currNode->branch[letterVal];                            //if we haven't reached the end of a branch and still have letters to go, move on
    }
    return currNode->isAWord;                                              //if we are out of letters and the node exists, return the isAWord value of that node
}

bool Dictionary::IsPrefix(string word) {
    Node* currNode = root;                                                 //set a traveling node pointer to the root

    for (int i = 0; i < word.size(); i++) {                                //go through each letter of a given word
        int letterVal = (int)word[i] - (int)'a';                           //calculate the integer value of each letter
        if (letterVal < 0 || letterVal > 25) {                             //check for invalid characters
            throw DictionaryError("Invalid character");               //throw an error if an invalid character is given
        }
        if(currNode->branch[letterVal] == nullptr) {                       //if we have reached the end of a branch and still have letters left
              return false;                                                //that means this must not be a prefix to a word
        }
        currNode = currNode->branch[letterVal];                            //move on to the next letter of the given word
    }
    return true;                                                           //we have found a valid path down the tree and have no more letter, this must be
}                                                                          //a valid prefix

int Dictionary::WordCount() const {
    return numWords;                                                       //gives the total number of words in our dictionary
}

void Dictionary::destroyHelper(Dictionary::Node *thisTree) {               // used for makeEmpty and the deconstructor
    if(thisTree == nullptr) {                                              //if there are no more nodes in the tree
        return;                                                            //leave the function
    }
    for(int i=0; i<NUM_CHARS; i++) {                                       //go through each possible letter a-z
        destroyHelper(thisTree->branch[i]);                                //call the destroyHelper function to restart the process
    }
    delete thisTree;                                                       //deletes all the nodes 1 by 1 after each node has been hit with the destroyHelper
}

void Dictionary::copyHelper(Dictionary::Node *&thisTree, Node *otherTree) { //Used for operator= which is used for the copy constructor
    if(otherTree ==  nullptr) {                                             //if the node in the tree we are trying to copy is empty
        thisTree = nullptr;                                                 //then make the node of our tree empty as well
        return;                                                             //leave the function
    }
    thisTree = new Node;                                                    //create a new node in our tree
    thisTree->isAWord = otherTree->isAWord;                                 //set the boolean flag in our tree's node to match the node of the tree we are copying
    for(int i=0; i<NUM_CHARS; i++) {                                        //go through each possible letter in the dictionary a-z
        copyHelper(thisTree->branch[i], otherTree->branch[i]);           //recall the copyHelper function with the next index value in the for loop
    }
}

void Dictionary::SaveDictionaryHelper(Dictionary::Node *curr, string currPrefix, ofstream &outFile) {  //Used for SaveDictionaryFile
    if(curr == nullptr) {                                                                              //if there are no more nodes
        return;                                                                                        //leave the function
    }
    if(curr->isAWord) {                                                                                //if we have finished a word
        outFile << currPrefix << endl;                                                                 //write the word on the document
    }
    for(int i=0; i<NUM_CHARS; i++) {                                                                   //go through each possible letter in the dictionary a-z
        char c = (char)(i+(int)'a');                                                                   //calculate the character value for the indices 0-25
        SaveDictionaryHelper(curr->branch[i], currPrefix+c, outFile);                               //recall the SaveDictionaryHelper with the new arguments
    }                                                                                                  //add the character we just calculated to our prefix string
}




//
// Created by we7289 on 2/24/2021.
//
#define CATCH_CONFIG_MAIN
#include "catch.hpp"
#include <fstream>
#include <vector>
#include <stdlib.h>
#include "../Dictionary.h"
#include "unit_util.h"


TEST_CASE("Dictionary Constructors") {
    SECTION("Default Constructor - 1pt") {
        Dictionary dict;

        // Should be 0
        REQUIRE(dict.WordCount() == 0);

        // None ot these should exist in the dictionary
        REQUIRE(!dict.IsWord(""));
        REQUIRE(!dict.IsWord("whatever"));
        REQUIRE(!dict.IsWord("fluffy"));
        REQUIRE(!dict.IsWord("banana"));
        REQUIRE(!dict.IsPrefix(""));
        REQUIRE(!dict.IsPrefix("whatever"));
        REQUIRE(!dict.IsPrefix("banana"));
        REQUIRE(!dict.IsPrefix("fluffy"));
    }

    SECTION("Constructor with file - 1pt") {
        int numChecks = 100;
        string dictName = "dictionary.txt";
        Dictionary dict(dictName);

        // Should be 0
        ifstream dictFile;
        dictFile.open(dictName);
        if (dictFile.fail()) {
            INFO(dictName << " did not open.  Did you move the file?");
            FAIL();
        }

        vector<string> words = read_words(dictName);

        // Make sure all the words are in the dictionary
        for (int i = 0; i < words.size(); i++) {
            REQUIRE(dict.IsWord(words[i]));
        }

        // This is not a very extensive test.
        // Passing this test does not mean your addWord or IsWord works.
    }

    SECTION("AddWord and IsWord test - 4 pts") {
        int numChecks = 1000;
        string word;
        string dictName = "dictionary.txt";

        // Loads words from dictionary into a vector
        vector<string> words = read_words(dictName);

        // Loads pokemon from file into a vector
        vector<string> pokemon = read_words("test_data/pokemon.txt");

        // Manually add words to dictionary
        Dictionary dict;
        for (int i = 0; i < words.size(); i++) {
            dict.AddWord(words[i]);
        }

        // Make sure all the words are in the dictionary
        for (int i = 0; i < words.size(); i++) {
            REQUIRE(dict.IsWord(words[i]));
        }

        // Check 1000 random words (make sure order does not affect IsWord)
        for (int i = 0; i < numChecks; i++) {
            // Get random word
            int randWordInd = rand()%words.size();
            string randWord = words[randWordInd];

            REQUIRE(dict.IsWord(randWord));
        }

        // Make sure pokemon are NOT in the dictionary.
        // Note some pokemon like "ditto" have been removed since
        // they actually are words found in a dictionary.
        for (int i = 0; i < pokemon.size(); i++) {
            REQUIRE(!dict.IsWord(pokemon[i]));
        }
    }

    SECTION("IsPrefix test - 2pts") {
        int numChecks = 1000;

        string dictName = "dictionary.txt";
        Dictionary dict(dictName);

        ifstream pokeFile;
        pokeFile.open("test_data/pokemon.txt");
        if (pokeFile.fail()) {
            INFO("pokemon.txt did not open.  Did you move the file?");
            FAIL();
        }

        // Loads words from file into a vector
        vector<string> words = read_words(dictName);


        // Make sure all the words are in the dictionary
        for (int i = 0; i < words.size(); i++) {
            REQUIRE(dict.IsWord(words[i]));
        }

        // Check 1000 random words
        for (int i = 0; i < numChecks; i++) {
            // Get random word and prefix
            int randWordInd = rand()%words.size();
            string randWord = words[randWordInd];
            string randPrefix = randWord.substr(0,rand()%randWord.length());

            REQUIRE(dict.IsPrefix(randPrefix));
        }
    }

    SECTION("LoadDictionary - 2pts") {
        int numChecks = 1000;
        string dictName = "dictionary.txt";

        // Create Dictionary and load file
        Dictionary dict;
        dict.LoadDictionaryFile(dictName);

        // Loads words from file into a vector
        vector<string> words = read_words(dictName);

        // Make sure all the words are in the dictionary
        // Assuming AddWord and IsWord works, then this should work fine.
        for (int i = 0; i < words.size(); i++) {
            REQUIRE(dict.IsWord(words[i]));
        }
    }

    SECTION("SaveDictionary - 2pts") {
        int numChecks = 1000;
        Dictionary dict("test_data/pokemon.txt");

        dict.SaveDictionaryFile("saved_pokemon_dictionary.txt");

        vector<string> poke_test = read_words("test_data/pokemon.txt");
        vector<string> poke_saved = read_words("saved_pokemon_dictionary.txt");

        // Make sure they both have the same number of words
        REQUIRE(poke_test.size() == poke_saved.size());

        for (int i = 0; i < poke_test.size(); i++) {
            REQUIRE(poke_test[i] == poke_saved[i]);
        }

    }

    SECTION("MakeEmpty - 2pts") {

    }


}